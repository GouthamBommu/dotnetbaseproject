﻿using AutoMapper;
using DotNetBaseProject.DAL.Models;
using DotNetBaseProject.PresentationLayer.Dtos.Requests;
using DotNetBaseProject.PresentationLayer.Dtos.Responses;
using System;

namespace DotNetBaseProject.Shared.Automappers
{
    public class QuoteProfile: Profile
    {
        public QuoteProfile()
        {
            CreateMap<Quote, QuoteResponse>()
                .ForMember(dest => dest.QuoteId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.WriterName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.MessageInfo, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            CreateMap<CreateQuoteRequest, Quote>()
                .IncludeBase<QuoteBaseRequest, Quote>()
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(_ => DateTime.UtcNow));

            CreateMap<UpdateQuoteRequest, Quote>()
                .IncludeBase<QuoteBaseRequest, Quote>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.QuoteId))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate));

            CreateMap<QuoteBaseRequest, Quote>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.WriterName))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.MessageInfo));
        }
    }
}
