﻿using DotNetBaseProject.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotNetBaseProject.BusinessLayer.Services.Abstract
{
    public interface IQuoteService
    {
        Task<IEnumerable<Quote>> GetAllAsync();
        Task<Quote> GetByIdAsync(long id);
        Task<Quote> CreateAsync(Quote quote);
        Task<Quote> UpdateAsync(Quote quote);
        Task DeleteAsync(Quote quote);
        Task<IEnumerable<Quote>> GetByKeywordAsync(string keyword);
    }
}
