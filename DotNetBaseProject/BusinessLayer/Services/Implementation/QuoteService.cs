﻿using DotNetBaseProject.BusinessLayer.Services.Abstract;
using DotNetBaseProject.DAL.Models;
using DotNetBaseProject.DAL.Repository.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace DotNetBaseProject.BusinessLayer.Services.Implementation
{
    public class QuoteService: IQuoteService
    {
        private readonly IUnitOfWork _unitOfWork;

        public QuoteService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Quote> CreateAsync(Quote quote)
        {
            await _unitOfWork.Quotes
                .AddAsync(quote)
                .ConfigureAwait(false);

            await _unitOfWork
                .CommitAsync()
                .ConfigureAwait(false);

            return quote;
        }

        public async Task DeleteAsync(Quote quote)
        {
            _unitOfWork.Quotes
                .Remove(quote);

            await _unitOfWork
                .CommitAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Quote>> GetAllAsync()
        {
            return await _unitOfWork.Quotes
                .GetAllAsync().ConfigureAwait(false);
        }

        public async Task<Quote> GetByIdAsync(long id)
        {
            return await _unitOfWork.Quotes
                .GetByIdAsync(id)
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Quote>> GetByKeywordAsync(string keyword)
        {
            return await _unitOfWork.Quotes
                .SearchForKeywordAsync(keyword)
                .ConfigureAwait(false);
        }

        public async Task<Quote> UpdateAsync(Quote quote)
        {
            _unitOfWork.Quotes
                .Update(quote);

            await _unitOfWork
                .CommitAsync()
                .ConfigureAwait(false);

            return quote;
        }


    }
}
