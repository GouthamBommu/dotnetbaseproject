﻿using DotNetBaseProject.DAL.Contexts;
using DotNetBaseProject.DAL.Repository.Abstract;
using System.Threading.Tasks;

namespace DotNetBaseProject.DAL.Repository.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _dataContext;

        private QuoteRepository _quoteRepository;

        public UnitOfWork(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQuoteRepository Quotes => _quoteRepository ??= new QuoteRepository(_dataContext);

        public async Task<int> CommitAsync()
        {
            return await _dataContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }
    }
}
