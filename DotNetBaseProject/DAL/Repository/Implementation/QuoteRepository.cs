﻿using DotNetBaseProject.DAL.Contexts;
using DotNetBaseProject.DAL.Models;
using DotNetBaseProject.DAL.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DotNetBaseProject.DAL.Repository.Implementation
{
    public class QuoteRepository : Repository<Quote>, IQuoteRepository
    {
        public QuoteRepository(DataContext dataContext): base(dataContext)
        {
        }


        public async Task<IEnumerable<Quote>> SearchForKeywordAsync(string keyword)
        {
            var pattern = @"\b" + keyword + @"\b";
            Regex rgx = new Regex(pattern);

            return (from q in Context.Quotes.ToList()
                    where rgx.IsMatch(q.Name) || rgx.IsMatch(q.Message)
                    select q
                   ).ToList();
        }
    }
}
