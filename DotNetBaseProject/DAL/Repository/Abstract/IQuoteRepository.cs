﻿using DotNetBaseProject.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotNetBaseProject.DAL.Repository.Abstract
{
    public interface IQuoteRepository : IRepository<Quote>
    {
        Task<IEnumerable<Quote>> SearchForKeywordAsync(string keyword);
    }
}
