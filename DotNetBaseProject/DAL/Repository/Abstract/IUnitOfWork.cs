﻿using System;
using System.Threading.Tasks;

namespace DotNetBaseProject.DAL.Repository.Abstract
{
    public interface IUnitOfWork: IDisposable
    {
        IQuoteRepository Quotes { get; }
        Task<int> CommitAsync();
    }
}
