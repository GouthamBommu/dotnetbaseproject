﻿using System;

namespace DotNetBaseProject.DAL.Models
{
    public class Quote
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
