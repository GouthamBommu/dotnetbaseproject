﻿using DotNetBaseProject.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace DotNetBaseProject.DAL.Contexts
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Quote> Quotes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Quote>()
                .Property(q => q.Name)
                .IsRequired();

            modelBuilder.Entity<Quote>()
                .Property(q => q.Message)
                .IsRequired()
                .HasMaxLength(150);

            modelBuilder.Entity<Quote>()
                .Property(q => q.CreatedDate)
                .HasDefaultValue(DateTime.UtcNow);

            modelBuilder.Entity<Quote>()
                .HasKey(q => q.Id);
        }
    }
}
