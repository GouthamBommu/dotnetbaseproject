using DotNetBaseProject.BusinessLayer.Services.Abstract;
using DotNetBaseProject.BusinessLayer.Services.Implementation;
using DotNetBaseProject.DAL.Contexts;
using DotNetBaseProject.DAL.Repository.Abstract;
using DotNetBaseProject.DAL.Repository.Implementation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DotNetBaseProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services
                .AddMvcCore()
                .AddCors(opt =>
                {
                    var list = new List<string>();
                    Configuration.GetSection("CorsPolicy:Origins").Bind(list);
                    opt.AddDefaultPolicy(builder =>
                    {
                        builder
                        .WithOrigins(list.ToArray())
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    });
                })
                .AddJsonOptions(opt => {
                    opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                })
                .AddFluentValidation(options => options.RegisterValidatorsFromAssemblyContaining<Startup>())
                .AddApiExplorer();

            // add versioning
            services.AddApiVersioning(o => {
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.AssumeDefaultVersionWhenUnspecified = true;
            });

            services
                .AddDbContext<DataContext>(options =>
                {
                    options
                        .UseMySQL(Configuration.GetConnectionString("DefaultConnection"));
                });

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IQuoteService, QuoteService>();

            services.AddSwaggerGen();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
