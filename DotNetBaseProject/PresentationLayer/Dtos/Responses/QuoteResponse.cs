﻿using System;

namespace DotNetBaseProject.PresentationLayer.Dtos.Responses
{
    public class QuoteResponse
    {
        public long QuoteId { get; set; }
        public string WriterName { get; set; }
        public string MessageInfo { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
