﻿using System;

namespace DotNetBaseProject.PresentationLayer.Dtos.Requests
{
    public class QuoteBaseRequest
    {
        public string WriterName { get; set; }
        public string MessageInfo { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
