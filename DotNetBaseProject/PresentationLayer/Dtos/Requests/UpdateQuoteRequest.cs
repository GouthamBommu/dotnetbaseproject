﻿namespace DotNetBaseProject.PresentationLayer.Dtos.Requests
{
    public class UpdateQuoteRequest: QuoteBaseRequest
    {
        public long QuoteId { get; set; }
    }
}
