﻿using DotNetBaseProject.PresentationLayer.Dtos.Requests;
using FluentValidation;

namespace DotNetBaseProject.PresentationLayer.Validations
{
    public class UpdateQuoteRequestValidator : QuoteBaseRequestValidator<UpdateQuoteRequest>
    {
        public UpdateQuoteRequestValidator()
        {
            RuleFor(r => r.QuoteId)
                .NotNull()
                .NotEmpty();
        }
    }
}
