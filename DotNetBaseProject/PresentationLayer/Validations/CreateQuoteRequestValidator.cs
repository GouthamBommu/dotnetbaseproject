﻿using DotNetBaseProject.PresentationLayer.Dtos.Requests;

namespace DotNetBaseProject.PresentationLayer.Validations
{
    public class CreateQuoteRequestValidator: QuoteBaseRequestValidator<CreateQuoteRequest>
    {
    }
}
