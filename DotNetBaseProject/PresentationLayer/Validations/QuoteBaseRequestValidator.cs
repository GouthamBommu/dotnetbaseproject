﻿using DotNetBaseProject.PresentationLayer.Dtos.Requests;
using FluentValidation;

namespace DotNetBaseProject.PresentationLayer.Validations
{
    public class QuoteBaseRequestValidator<T>: AbstractValidator<T> where T : QuoteBaseRequest
    {
        public QuoteBaseRequestValidator()
        {
            RuleFor(r => r.WriterName)
                .NotNull()
                .NotEmpty();

            RuleFor(r => r.MessageInfo)
                .Must(mi => mi.Split("").Length <= 150)
                .NotNull()
                .NotEmpty();
        }
    }
}
