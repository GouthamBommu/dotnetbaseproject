﻿using AutoMapper;
using DotNetBaseProject.BusinessLayer.Services.Abstract;
using DotNetBaseProject.DAL.Models;
using DotNetBaseProject.PresentationLayer.Dtos.Requests;
using DotNetBaseProject.PresentationLayer.Dtos.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotNetBaseProject.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[Controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {
        private readonly IQuoteService _quoteService;
        private readonly IMapper _mapper;

        public QuotesController(IQuoteService quoteService, IMapper mapper)
        {
            _quoteService = quoteService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<QuoteResponse>>> GetAllAsync()
        {
            try
            {
                var quotes = await _quoteService.GetAllAsync().ConfigureAwait(false);
                var quoteResponses = _mapper.Map<IEnumerable<QuoteResponse>>(quotes);
                return Ok(quoteResponses);
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpPost]
        public async Task<ActionResult<QuoteResponse>> CreateQuoteAsync(CreateQuoteRequest request)
        {
            try
            {
                var quote = _mapper.Map<Quote>(request);

                quote = await _quoteService
                    .CreateAsync(quote).ConfigureAwait(false);

                return Ok(quote);
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpPut]
        public async Task<ActionResult<QuoteResponse>> UpdateQuoteAsync(UpdateQuoteRequest request)
        {
            try
            {
                var dbquote = await _quoteService
                    .GetByIdAsync(request.QuoteId).ConfigureAwait(false);

                if (dbquote == null)
                {
                    return NotFound(new { error = $"Quote: {request.QuoteId} doesn't exists" });
                }

                var updatedQuote = _mapper.Map<UpdateQuoteRequest, Quote>(request, dbquote);
                updatedQuote = await _quoteService
                    .UpdateAsync(updatedQuote).ConfigureAwait(false);

                return Ok(updatedQuote);
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<QuoteResponse>> DeleteQuoteAsync(long id)
        {
            try
            {
                if (id == 0)
                {
                    throw new Exception($"id should be grater than 0");
                }

                var quote = await _quoteService
                    .GetByIdAsync(id).ConfigureAwait(false);

                if (quote == null)
                {
                    return NotFound(new { error = $"Quote: {id} doesn't exists" });
                }

                await _quoteService
                    .DeleteAsync(quote).ConfigureAwait(false);

                return Ok(quote);
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpGet("keywordSearch")]
        public async Task<ActionResult<IEnumerable<QuoteResponse>>> GetByKewordAsync([FromQuery] string keyword)
        {
            try
            {
                if (keyword.Split(" ").Length > 1)
                {
                    return BadRequest("Keyword should contain only one word");
                }

                var quotes = await _quoteService
                    .GetByKeywordAsync(keyword)
                    .ConfigureAwait(false);

                var quoteResponses = _mapper.Map<IEnumerable<QuoteResponse>>(quotes);

                return Ok(quoteResponses);
            }
            catch (Exception e)
            {
                return BadRequest(new { error = e.Message });
            }
        }
    }
}
