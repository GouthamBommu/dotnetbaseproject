# DotNetBaseProject

### Documentation:
I have built this service on .Net core and ef core ORM, which is being used to create and manage mysql database.

### Technologies Used
- .Net core
- ORM (ef core)
- Docker (To build the api service image)
- Docker Compose (To run mysql db, phpMyAdmin, api service in docker containers)

### Install dependencies
- Install docker and docker-compose on your system.
- Install the ef client (ORM tool) to create and manage database: **dotnet tool install --global dotnet-ef**

### How to build/run application
- Clone the repository on your system and open the root folder.
- Launch database and service as docker containers by follwoing below instructions
    - run command: **docker-compose up -d**
    - To view database: **http://localhost:8080/**
        - username: root
        - password: 192837
- Run database migrations to create tables in a mysql docker container.
    - run command: **dotnet ef core database update**
- Now the system is configured for testing
- To test the solution, open the swagger ui by following below instructions:
    - **http://localhost:5000/swagger**
    - execute api's
    

