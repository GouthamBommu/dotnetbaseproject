FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY SolutionItems/NuGet.Config ./
COPY DotNetBaseProject/*.csproj ./
RUN dotnet restore

COPY DotNetBaseProject/ ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine
WORKDIR /app
EXPOSE 80
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "DotNetBaseProject.dll"]